<!-- serving PHP files:
php -S localhost:8000 -->


<?php require_once "./code.php";?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S01: PHP Basics and Selection</title>
</head>
<body>

	<!-- <h1>Hello World</h1> -->

	<h1>Echoing Values</h1>

	<!-- single quoute cant outpus variable values -->
	<p><?php echo 'Good day ' . $name . '!' . 'Your given email is ' . $email . '.'; ?>
	<p><?php echo 'Good day $name! your given email is $email'; ?></p>

	<!-- double quoute can easily read variables -->
	<p><?php echo "Good day $name! your given email is $email"; ?></p>

	<p><?php echo PI;?></p>

	<h1>Data types</h1>
	<p><?php echo $hasTraveledAbroad; ?></p>
	<p><?php echo $spouse; ?></p>

	<!-- to see their types we use var_dump() -->

	<p><?php echo gettype($hasTraveledAbroad); ?></p>
	<p><?php echo gettype($spouse); ?></p>

	<p><?php echo var_dump($hasTraveledAbroad); ?></p>
	<p><?php echo var_dump($spouse); ?></p>

	<p><?php var_dump($gradeObj)?></p>
	<p><?php echo $gradeObj->firstGrading; ?></p>
	<p><?php echo $personObj->address->state; ?></p>

	<p><?php echo $grades[3]; ?></p>
	<p><?php echo $grades[2]; ?></p>


	<h1>Operators</h1>

	<p>X: <?php echo $x; ?></p>
	<p>Y: <?php echo $y; ?></p>

	<p>is legal age: <?php var_dump($isLegalAge);?></p>
	<p>is registered: <?php var_dump($isRegistered); ?></p>

	<h2>Arithmetic Operators</h2>
	<p>Sum: <?php echo $x + $y; ?></p>
	<p>Difference: <?php echo $x - $y; ?></p>
	<p>Product: <?php echo $x * $y; ?></p>
	<p>Qoutient: <?php echo $x / $y; ?></p>
	<p>Modulo: <?php echo $x % $y; ?></p>

	<h2>Equality Operator</h2>
	<p>Loose Equality: <?php var_dump($x == '1342.14')?> </p>
	<p>Strict Equality: <?php var_dump($x === '1342.14')?> </p>
	<p>Loose Inequality: <?php var_dump($x != '1342.14')?> </p>

	<h2>Greater/Lesser Operator</h2>
	<p>Is Lesser: <?php var_dump($x < $y); ?></p>
	<p>Is Lesser: <?php var_dump($x > $y); ?></p>
	<p>Is Lesser or Equal: <?php var_dump($x <= $y); ?></p>
	<p>Is Greater or Equal: <?php var_dump($x >= $y); ?></p>


	<h2>Logical Operators</h2>
	<p>All Requirements Met: <?php var_dump($isLegalAge && $isRegistered); ?></p>
	<p>Some Requirements Met: <?php var_dump($isLegalAge || $isRegistered); ?></p>
	<p>Some Requirements Not Met: <?php var_dump($isLegalAge && !$isRegistered); ?></p>


	<p>Full Name: <?php echo getFullName('John', 'D', 'Smith') ;?></p>

	<h1>Selection Control Structure</h1>
	<h2>If-ElseIf-Else</h2>

	<p><?php echo determineTyphoonIntensity(12); ?></p>
	<h2>Ternary Sample</h2>


	<h2>Try-Catch-Finally</h2>











</body>
</html>