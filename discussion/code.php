<?php

// [section] variables
// variables are defined using ($) notation before the name of the variable

$name = "John Smith";
$email = "johnsmith@gmail.com";

// [section] Constants
// defined using the "define()"
//doesnt use $ notation

define('PI', 3.1416);

// strings
$state = 'New York';
$country = 'United States of America';
$address = $state .', '.$country; //dot operator
$address = "$state, $country";


//integer
$age = 31;
$headcount = 26;

//floats
$grade = 98.3;
$distanceInKilometers = 1243.45;

// boolean
$hasTraveledAbroad = false;
$haveSymptoms = true;

// arrays
$grades = array(98.1, 89.22, 87.50, 99.5);

// null
$spouse = null;
$middleName = null;

//object
$gradeObj = (object)[
	'firstGrading'  => 98.1,
	'secondGrading' => 89.2,
	'thirdGrading'  => 90.2,
	'fourthGrading' => 93.6
];

$personObj = (object)[
	'fullName'  => 'John Smith',
	'isMarried' => false,
	'age'       => 35,
	'address'   => (object) [
		'state' => 'New York',
		'country' => 'United States of America'
	]
];

// [section] Operators

// assignment operator

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;


// [Section]
function getFullName($firstName, $middleinitial, $lastName){
	return "$lastName $firstName $middleinitial";
}

// if-Elseif-else
function determineTyphoonIntensity($windSpeed){
	if($windSpeed <30){
		return "not a typhoon yet";
	} else if($windSpeed <=61){
		return "tropical storm detected";
	} else if ($windSpeed >=89 && $windSpeed <= 117){
		return "severe tropical storm detected";
	} else {
		return "typhoon detected";
	}
}
function isUnderAge($age){
	return ($age < 18) ? "legal age" : "underage";
}

// try-catch-finally
function greeting($str){
	try {
		if(gettype($str) === "string"){
			echo $str;
		}
	} else  {
		throw new Exception("Oops!");		
	}
	catch (Exception) 
}

















