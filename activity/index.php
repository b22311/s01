
<?php require_once "./code.php";?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity S01</title>
</head>
<body>

	<h1>Full Address</h1>
	<p><?php echo getFullAddress("Philippines", "Cebu City", "Cebu", "block 20, lot 7, Metallica Village");?></p>

	<h1>Get Letter Grade</h1>
	<p><?php echo getLetterGrade(85); ?></p>
</body>
</html>